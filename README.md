# PhotoframeFixer

Small python script that fixes image pixels per unit (resolution) to allow them to be displayed on an old digital photo frame.

Requires Imagemagick 7.x.x commands,
but will work with Python 3.6+.
