#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This program is free software: you can redistribute it and/or
modify it under the terms of version 3 of the GNU General Public
License as published by the Free Software Foundation.                                                                               This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.

You should have received a copy of the GNU General Public 
License along with this program.  If not, see 
<https://www.gnu.org/licenses/>.
"""

import sys, subprocess, logging

from pathlib import Path
from argparse import ArgumentParser

if __name__ == "__main__":
    parser = ArgumentParser()

    parser.description = "Script to make pictures compatible with Pink Photo Frame."

    parser.add_argument("filename", nargs="+", help="picture file(s)")
    parser.add_argument("-d", "--debug", help="turn debugging on", action="store_true")
    parser.add_argument(
        "-i", "--inplace", help="use mogrify instead of magick", action="store_true"
    )

    args = parser.parse_args()

    if args.debug is True:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    logging.basicConfig(level=log_level, format="%(levelname)s - %(message)s")

    for infile in args.filename:
        resolved = Path(infile).resolve()

        if resolved.exists() is False:
            logging.error(f"File: {resolved} does not exist!")

            sys.exit(1)

        try:
            cmdline = ["magick", "identify", "-format", "%x,%y,%w,%h", f"{resolved}"]

            if sys.version_info.minor <= 6:
                result = subprocess.run(
                    cmdline, stdout=subprocess.PIPE, encoding="utf-8"
                )

            else:
                result = subprocess.run(cmdline, capture_output=True, encoding="utf-8")

            logging.debug(f"{result.stdout=}")

            resX, resY, width, height = result.stdout.split(",")

            resX = float(resX)
            resY = float(resY)
            width = int(width)
            height = int(height)

            if resX > 96.0 or resY > 96.0:
                outfilename = f"{resolved.stem}-corrected{resolved.suffix}"
                outfile = Path(resolved.parent).joinpath(outfilename).resolve()

                argslist = []

                if args.inplace is True:
                    argslist = [
                        "magick",
                        "mogrify",
                        "-resample",
                        "96x96",
                        "-scale",
                        f"{width}x{height}",
                        f"{resolved}",
                    ]

                else:
                    logging.debug(f"{outfile=}")

                    argslist = [
                        "magick",
                        f"{resolved}",
                        "-resample",
                        "96x96",
                        "-scale",
                        f"{width}x{height}",
                        f"{outfile}",
                    ]

                logging.debug(f"{argslist=}")

                result = subprocess.run(argslist)

                if result.returncode == 0:
                    if args.inplace is True:
                        logging.info(f"{resolved} is now frame-ready.")
                    else:
                        logging.info(f"{outfile} is now frame-ready.")

        except KeyboardInterrupt:
            print()
            sys.exit(0)
